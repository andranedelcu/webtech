const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
var unirest = require('unirest');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(express.static('..proiect/build'));

const sequelize = new Sequelize('c9', 'neagualexandru', '', {
   host: 'localhost',
   dialect: 'mysql',
   operatorsAliases: false,
   pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    }
});

sequelize.authenticate().then(() => {
    console.log('Conexiune reusita!');
  })
  .catch(err => {
    console.error('Conexiune nereusita:', err);
  });
  
  const Utilizator = sequelize.define('utilizatori', {
  username: {
       type: Sequelize.STRING,
       allowNull: false,
       primaryKey: true
   }, 
   nume: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   prenume: {
        type: Sequelize.STRING,
        allowNull: false
   },
  
   parola: {
       type: Sequelize.STRING,
       allowNull: false
   },
   
   domiciliu:
   {
       type: Sequelize.STRING,
       allowNull: false
   }
});

const Oras= sequelize.define('orase',
{
   id_oras: {
       type: Sequelize.INTEGER,
       autoIncrement:true,
       allowNull:false,
       primaryKey:true
   },
   
   denumire:{
     type: Sequelize.STRING,
     allowNull:false
   },
    
    tara:{
        type: Sequelize.STRING,
        allowNull:false
    }
    
});


const Atractie=sequelize.define('atractii',{
    id_atractie:
    {
        type: Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true
    },
    
    latitudine:
    {
        type: Sequelize.DOUBLE,
        allowNull:false    
        
    },
    
    longitudine:
    {
        type: Sequelize.DOUBLE,
        allowNull:false   
    },
    
    descriere:
    {
        type: Sequelize.STRING,
        allowNull:false    
    }
    
});

let atractii = [
    {
        id: 0,
        latitudine: 43,
        longitudine:23,
        descriere:'Ana are mere'
    },
    {
        id: 1,
        latitudine: 43,
        longitudine:23,
        descriere:'Ana are mere'
    },
    {
        id: 2,
       latitudine: 43,
        longitudine:23,
        descriere:'Ana are mere'
    },
    {
        id: 3,
        latitudine: 43,
        longitudine:23,
        descriere:'Ana are mere'
    },
    {
        id: 4,
        latitudine: 43,
        longitudine:23,
        descriere:'Ana are mere'
    },
    {
        id: 5,
        latitudine: 43,
        longitudine:23,
        descriere:'Ana are mere'
    }
];

const Camera= sequelize.define('camere',{
    id_camera:
    {
        type: Sequelize.INTEGER,
        allowNull:false,
        primaryKey:true,
        autoIncrement:true
    },
    
    stare:
    {
        type: Sequelize.BOOLEAN,
        allowNull:false
    }
});

const Istoric= sequelize.define('istoric',
{
    username:
    {
        type: Sequelize.STRING,
        allowNull:false
        
    },
    
    id_atractie:
    {
        type: Sequelize.INTEGER,
        allowNull: false
    }
    
});

Oras.hasMany(Atractie);
Atractie.hasMany(Camera);


sequelize.sync({force: true}).then(()=>{
    console.log('A fost creata baza de date.');
});


app.post('/login', (req, res) => {
   Utilizator.findOne({where:{username: req.body.username, parola: req.body.parola} }).then((result) => {
       if(result!=null){
       res.status(200).send(result);
       }else{
           res.status(500).send('Error');
       }
   }) ;
});
 
 
 app.post('/signup', (req, res, next) => {
  Utilizator.create(req.body)
    .then(() => res.status(201).send('Utilizator adaugat.'))
    .catch((err) => next(err));
});


app.get('/users', (req, res, next) => {
  Utilizator.findAll()
    .then((utilizatori) => res.status(200).json(utilizatori))
    .catch((err) => next(err));
});

 
 app.put('/utilizatori/:username', (req, res, next) => {
  Utilizator.findById(req.params.username)
    .then((utilizatori) => {
      if (utilizatori){
        return utilizatori.update(req.body, {fields : ['username','nume','prenume', 'parola', 'domiciliu']});
      }
      else{
        res.status(404).send('Nu exista acest utilizator in baza de date.');
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('S-au actualizat datele pentru acest utilizator.');
      }
    })
    .catch((err) => next(err));
});
 
 app.delete('/utilizatori/:username', (req, res, next) => {
  Utilizator.findById(req.params.username)
    .then((utilizatori) => {
      if (utilizatori){
        return utilizatori.destroy();
      }
      else{
        res.status(404).send('Nu exista acest utilizator in baza de date.');
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('S-au sters datele utilizatorului.');
      }
    })
    .catch((err) => next(err));
});



 app.get('/atractii', (req,res) =>{
         res.status(200).json(atractii);    
 });
 
 app.put('/atractii/:id', (req, res, next) => {
     res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8081');
     res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080');
  Atractie.findById(req.params.id_atractie)
    .then((atractii) => {
      if (atractii){
        return atractii.update(req.body, {fields : ['id_atractie','latitudine','longitudine', 'descriere']});
      }
      else{
        res.status(404).send('Nu exista aceasta atractie turistica in baza de date.');
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('S-au actualizat datele pentru aceasta atractie turistica.');
      }
    })
    .catch((err) => next(err));
});

 app.post('/istoric', (req, res) =>{
     Istoric.create({
         username: req.body.username,
         id_atractie:req.body.id_atractie
     }).then((istoric) => {
         res.status(200).send("Adaugat la istoric.");
     }, (err) => {
         res.status(500).send(err);
     });
 });
 
 app.get('/getistoric', (req, res, next) => {
  Istoric.findAll()
    .then((istoric) => res.status(200).json(istoric))
    .catch((err) => next(err));
});



unirest.get("https://webcamstravel.p.rapidapi.com/webcams/list/category={category}?lang=en&show=webcams%3Aimage%2Clocation")
.header("X-RapidAPI-Key", "WEM9yias4imshqlpbDX26SD24YFtp1prws8jsnKuBQWQ41qalg")
.end(function (result) {
  console.log( result.body);
});

unirest.get("https://webcamstravel.p.rapidapi.com/webcams/map/57.520542,3.796860,36.886649,39.796890,5")
.header("X-RapidAPI-Key", "WEM9yias4imshqlpbDX26SD24YFtp1prws8jsnKuBQWQ41qalg")
.end(function (result) {
  console.log(result.body);
});




app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
});












