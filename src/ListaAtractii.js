import React, { Component } from 'react';
import './App.css';


class ListaAtractii extends Component {
  
  transformItems = () => {
        switch(this.props.title){
            case "Lista Atractii":
                return this.props.source.map((element, index) => {
                    return <div key={index}>{element.descriere}</div>
                })
        
            default:
                return this.props.source.map((element, index) => {
                    return <div key={index}>{element.descriere}</div>
                })
        }
         
    }
  
  
  render() {
         
          let items = this.transformItems();
        
        return (
            <div>
            <h1>{this.props.title}</h1>
                {items}
            </div>
            );
   
  }
}

export default ListaAtractii;