import React, { Component } from 'react';
import './App.css';
import Login from "./Login";
import Orase from "./Orase";
import Atractii from "./Atractii";

import 
{ Route,NavLink,HashRouter } from "react-router-dom";

class App extends Component {
  
  
  render() {
    
    
    return (
      <HashRouter>
      
      <div className="meniu">
      
      <div className="textemeniu">
        <ul> 
          <li><NavLink to="/">Login-Autentificare</NavLink></li>
          <li><NavLink to="/orase">Orase</NavLink></li>
          <li><NavLink to="/atractii">Atractii turistice</NavLink></li>
        </ul>
        </div>
        
         <div className="meniuContinut">
          <Route exact path="/" component={Login}/>
          <Route path="/orase" component={Orase}/>
          <Route path="/atractii" component={Atractii}/>
         </div>
     
      </div>
      </HashRouter>
      
      
    );
  }
}
export default App;
